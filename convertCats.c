///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file convertCats.c 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   8_Mar_2022
///////////////////////////////////////////////////////////////////////////////
#include <string.h>

#include "catDatabase.h"
#include "convertCats.h"

char* genderName( const enum Gender gen ) {

   static char gender[MAX];

   switch( gen )
   {
      case 0:
         strcpy(gender, "Unknown");
         break;
      case 1:
         strcpy(gender, "Male");
         break;
      case 2:
         strcpy(gender, "Female");
         break;
   }

   return gender;

}

char* breedName( const enum Breed type ) {

   static char breed[MAX];

   switch( type )
   {
      case 0:
         strcpy(breed, "Unknown");
         break;
      case 1:
         strcpy(breed, "Maine Coon");
         break;
      case 2:
         strcpy(breed, "Manx");
         break;
      case 3:
         strcpy(breed, "Shorthair");
         break;
      case 4:
         strcpy(breed, "Persian");
         break;
      case 5:
         strcpy(breed, "Sphynx");
         break;
   }

   return breed;

}


char* colorName( const enum Color col ) {

   static char color[MAX];

   switch( col )
   {
      case 0:
         strcpy(color, "Black");
         break;
      case 1:
         strcpy(color, "White");
         break;
      case 2:
         strcpy(color, "Red");
         break;
      case 3:
         strcpy(color, "Blue");
         break;
      case 4:
         strcpy(color, "Green");
         break;
      case 5:
         strcpy(color, "Pink");
         break;
   }

   return color;

}











