///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file convertCats.h
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   8_Mar_2022
///////////////////////////////////////////////////////////////////////////////
#define MAX (50)

extern char* genderName( const enum Gender gender ) ;
extern char* breedName( const enum Breed type ) ;
extern char* colorName( const enum Color col ) ;
