///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.h 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   8_Mar_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

void printCat( int index );
void printAllCats();
int findCat( char name[]);
